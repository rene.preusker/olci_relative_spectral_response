# Calculation of OLCI's relative spectral responses

The tool provides few lines of code to calculate pixel-by-pixel relative spectral responses (RSR) for OLCI. It makes use of OLCI's temporal spectral model and a good-as-possible inflight spectral characterization.  **From 4th collection on the centre wavelength and full-widh-half-maximum provided with L1b are the same, as they are calculated herin. Earlier collections do only provide pre-launch values. They should not be used! It is save to always use the spectral model calulated with this tool**

## Background
Each  OLCI instrument is made of five imaging spectrometer, equipped with a CCD that collects  740 columns 
in the spatial dimension across the camera field of view and 520 rows in the spectral dimension.
Spectral bands are created by binning several allong track rows.
One of OLCI peculiarities is a spectral response change of the CCD rows across the camera field of 
view of up to 1.8nm ('spectral smile'). This change is not constant in time but it shows temporal 
evolutions (up to 0.3nm since launch). These variations are fully incorporated in the radiometric 
calibration, thus they are  uncritical for most aplications eventually working with top of atmosphere 
reflectances. 

But a precise as possible characterisation is necessary for remote sensing algorithms using features with strong spectral gradients. It is indispensable for the OLCI bands 13, 14 and 15 at 760 nm, surrounding the O2-A
absorption. It may also be important for the water vapour absorption bands 19 and 20, for the very blue
Rayleigh bands 1 and 2 at 400 nm, for the chlorophyll fluorescence bands, the red-edge bands around 750
nm and others. 
It turned out that for these cases, determining the spectral response can be confusing and complicated, especially if users don't have a detailed insight on how OLCI works. This tool helps, by calculating centre wavelength, FWHM and the relative spectral response for every pixel, from band number (1,...,21), SAFE file name (used to calculatee orbit number) and  _detetector_index_. Detector index can be found in instrument.nc of every L1b and L2 SAFE file.    


## Installation
### Prerequisites
* A reasonably up-to-date python version and the following modules
* numpy
* netCDF4

For the demo in howto aditionally:
* matplotlib
* jupyter
* scipy

## Usage
* see **_howto_use.ipynb_**.
There are two options:
  * The first one is based on _S3[A|B]_OL_SRF_20230331_mean_rsr.nc4_ It takes mean responses and adapt them by a simple scale and shift. 
  * The second uses the pixel-by-pixel RSRs (_S3[A|B]_OL_SRF_20230331.nc4_) and scale & shift them inidividually. I can't currently think of any application that needs such a procedure, not even the O2A bands. What's more, this procedure could pretend an accuracy that doesn't exist. The inflight estimation of the RSRs is based on many ground-based characterizations too, which we can not check inflight (afaik).

It is safe to use the first option, but may test both and look for differences. 


## Roadmap
Update of the technical notes.  
Depends on feedback

## Authors and acknowledgment
Rene  Preusker

## License

## Project status
Ready to be tested in the wild
