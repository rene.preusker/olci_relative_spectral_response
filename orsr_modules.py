from scipy.optimize import bisect
from scipy.interpolate import interp1d
import numpy as np
from netCDF4 import Dataset 
from pathlib import Path

F2OCONF = {'A':{'cycle_offset':-2,'orbit_offset':358},
           'B':{'cycle_offset':-20,'orbit_offset':3352}
           }

NOMINAL = np.array([400.,412.5,442.5,490.,510.,560.,620., 665.,
                    673.75,681.25,708.75,753.75, 761.25,764.375,
                    767.50,778.75, 865, 885, 900,940., 1015.])


def fn2orbit(fn_):
    '''
    Sentinel SAFE filename convention is: 
        MMM_SS_L_TTTTTT_YYYYMMDDTHHMMSS_YYYYMMDDTHHMMSS_YYYYMMDDTHHMMSS_<instance ID    >_GGG_<class ID>
        second and third field of <instance ID    > are cycle and relative orbit ....
    Args:
        fn_ (str or Path): SEN3 file name
    Returns:
        orbit (int): orbit number
    '''

    fn = Path(fn_)
    instance_id = fn.stem[64:64+17].split('_')

    # cycle number
    ccc = int(instance_id[1])
    # relative orbit
    rlo = int(instance_id[2])
    #olci 
    olci = fn.stem[2]
    orbit = ((ccc+F2OCONF[olci]['cycle_offset'])*385
             + rlo
             + F2OCONF[olci]['orbit_offset'])
    return orbit

def detectorindex2camera(didx):
    return didx // 740
    
def detectorindex2ccdindex(didx):
    if isinstance(didx,int):
        if didx >=0:
            ccdi = 739 - didx%740
        else:
            ccdi = -1 
    elif isinstance(didx,np.ndarray):
        good = didx >= 0
        ccdi = np.zeros_like(didx)-1
        ccdi[good] = didx[good]%740
        ccdi[good] = 739 - ccdi[good]
    else:
        print('aii ai ai')
    return ccdi

def fill_if_masked(d):
    try:
        return d.filled()
    except Exception as eee:
        #print(eee)
        return d

def read_all_from_ncdf(fn):
    out = {}
    with Dataset(fn) as ds:
        for v in ds.variables:
            out[v] = fill_if_masked(ds[v][:])
        out['attributes'] = {}
        for a in ds.ncattrs():
            out['attributes'][a] = ds.__getattr__(a)
    return out

def center_moment(wvl,sha):
    '''
    defined as first moment of rsr
    '''
    return np.trapz(sha*wvl,wvl)/np.trapz(sha,wvl)

def bandwidth_fwhm(wvl,sha_, hf=0.5, convolve=False):
    '''
    Verbatim defined, but can be adapted with hf=0.5
    aditionally using a 1.25nm convolution to avoid jumps in fwhm (only for Oa21)
    '''
    sha = sha_*1.
    if convolve:
        num_convolve=int(np.ceil(1.25/((wvl[-1]-wvl[0])/wvl.size)))
        #print('num_convolve',num_convolve)
        sha = np.convolve(sha,np.ones(num_convolve),'same')
    sha = sha/sha.max()
    mxx = np.argmax(sha)
    
    frsp = interp1d(wvl,sha)
    ffrsp = lambda x: frsp(x)-hf
    return bisect(ffrsp,wvl[mxx],wvl[-1])-bisect(ffrsp,wvl[0],wvl[mxx])

def shift_and_scale_wvl(orig_wvl:np.ndarray
                        ,orig_cwvl:float
                        ,orig_fwhm:float 
                        ,new_cwvl:float
                        ,new_fwhm:float
                       )->np.ndarray:
    '''
    Input: 
        orig_wvl:     original wvl-vector of SRF
        orig_cwvl:    original centre wavelength of SRF
        orig_fwhm:    original full width at half maximum of SRF
        new_cwvl:     new centre wavelength of SRF
        new_fwhm:     new full width at half maximum of SRF
    Output:
        new_wvl:      new wvl-vector of SRF
    '''
    delta_cwvl = new_cwvl-orig_cwvl  
    scale_fwhm = new_fwhm/orig_fwhm
    
    new_wvl = orig_wvl + delta_cwvl 
    new_wvl = (new_wvl - new_cwvl) * scale_fwhm + new_cwvl
    
    return new_wvl

class rsr_from_mean():
    
    def __init__(self,fn):
        self.SRF = read_all_from_ncdf(fn)

    def calculate_rsr(self, bn, cwl, fwhm):
        '''
        bn:     1,...,21
        cwl:    new central wavelelngth
        fwhm:   new fwhm
        '''
        mrsr = self.SRF['mean_spectral_response_function'][bn-1]
        mwvl = self.SRF['mean_spectral_response_function_wavelength'][bn-1]
        ocwl = self.SRF['srf_centre_wavelength'][bn-1]
        ofwh = self.SRF['bandwidth_fwhm'][bn-1]
        nwvl = shift_and_scale_wvl(mwvl,ocwl,ofwh,cwl,fwhm)
        
        return (nwvl,mrsr)

class rsr_from_pixel():

    def __init__(self,fn):
        self.SRF = read_all_from_ncdf(fn)

    def calculate_rsr_(self,bn,cam,cid,cwl,fwhm):
        '''
        bn:     1,...,21
        cam:    0,...,4
        cid:    739,...,0.  # ccd index
        cwl:    new central wavelelngth
        fwhm:   new fwhm
        '''
        mrsr = self.SRF['spectral_response_function'][bn-1,cam,cid]
        mwvl = self.SRF['spectral_response_function_wavelength'][bn-1,cam,cid]
        ocwl = self.SRF['centre_wavelength'][bn-1,cam,cid]
        ofwh = self.SRF['bandwidth_fwhm'][bn-1,cam,cid]
        nwvl = shift_and_scale_wvl(mwvl,ocwl,ofwh,cwl,fwhm)        
        return (nwvl,mrsr)
        
    def calculate_rsr(self,bn,cwl,fwhm,didx):
        '''
        bn:     1,...,21
        det:    0,...,3699   # detector index
        cwl:    new central wavelelngth
        fwhm:   new fwhm
        '''
        cam = detectorindex2camera(didx)
        cid = detectorindex2ccdindex(didx)
        return self.calculate_rsr_(bn,cam,cid,cwl,fwhm)


class temporal_model():
    def __init__(self,fn):
        self.EVO = read_all_from_ncdf(fn)
        # easy memoizing
        self.cache = {}
        
    def _coef_orbit2values(self,coef,orbit):
        '''
        coef: polynomial coefficients from LUT
        orbitnumber: 
        
        returns values for a given orbit 
        in a format compatible to L1b 
        '''
        lob = np.log(orbit)
        val = np.array([coef[i,...]*lob**i for i in (0,1,2)]).sum(axis=0)
        return np.transpose(val,(1,0,2))[...,::-1].reshape((21,5*740))

    def _cwl_and_fwhm_from_orbit(self,orbit):
        if orbit < self.EVO['attributes']['transition_orbit']:
            cwl_coef = self.EVO['cwvl_coef_early']
            fwh_coef = self.EVO['fwhm_coef_early']
        else:
            cwl_coef = self.EVO['cwvl_coef']
            fwh_coef = self.EVO['fwhm_coef']
        out = {}  
        out['cwl'] = self._coef_orbit2values(cwl_coef,orbit)
        out['fwh'] = self._coef_orbit2values(fwh_coef,orbit)
        return out 
        
    def cwl_and_fwhm_from_orbit(self,orbit,bn,didx):
        '''
        bn:     1,...,21
        det:    0,...,     # detector index
        orbit:  number
        '''
        if orbit not in self.cache:
            self.cache[orbit] = self._cwl_and_fwhm_from_orbit(orbit)
        return self.cache[orbit]['cwl'][bn-1,didx], self.cache[orbit]['fwh'][bn-1,didx] 